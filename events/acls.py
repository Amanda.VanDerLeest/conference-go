import requests
import json

from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY

pexel_headers = {"Authorization": PEXELS_API_KEY}

weather_headers = {"Authorization": OPEN_WEATHER_API_KEY}


def find_pictures(search_term):
    pexels_url = "https://api.pexels.com/v1/search?query=" + search_term
    response = requests.get(pexels_url, headers=pexel_headers)

    response = json.loads(response.content)
    return response["photos"][0]["src"]["original"]
    # return {"picture_url"}
